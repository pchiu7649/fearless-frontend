import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const[name, setName] = useState('');
    const[start, setStart] = useState('');
    const[end, setEnd] = useState('');
    const[description, setDescription] = useState('');
    const[presentations, setPresentations] = useState('');
    const[attendees, setAttendees] = useState('');
    const[location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setPresentations(value);
    }
    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;

        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response;
            console.log(newConference)
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setLocations([...data.locations]);
            console.log(locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">

                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartChange} placeholder="starts" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndChange} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea onChange={handleDescriptionChange} className="form-control" name="description" id="description" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePresentationsChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} name="location" required id="location" className="form-select">
                    <option defaultValue="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    );
}

export default ConferenceForm;