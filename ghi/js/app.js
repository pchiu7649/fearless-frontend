function createCard(name, description, pictureUrl, startDate, endDate, location) {

    const localStart = new Date(startDate)
    const localEnd = new Date(endDate)

    return `
    <div class="col">
        <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded ms-1">
            <img src="${pictureUrl}" class="card-img-top" alt="${name} picture">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer">${localStart.toLocaleDateString()} - ${localEnd.toLocaleDateString()}</div>
            </div>

        </div>
    </div>
    `;
  }

function throwError(e) {


    return `
        <div class="alert alert-danger" role="alert">
        ${e}
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try{
        const response = await fetch(url);

        if (!response.ok){
            //figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }


        }

    } catch (e) {
        //figure out what to do if an error is raised
        const errorMessage = document.querySelector(".error")

        console.error(e);
        let htmlError = throwError(e);
        errorMessage.innerHTML += htmlError;
    }



});
