window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    try{

        const response = await fetch(url);

        if (!response.ok) {
            //figure out what to do if the response is bad
        } else {
            const data = await response.json();
            //Parent element
            const selectTag = document.getElementById('location');

            for (let location of data.locations) {
                //Child Element
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                //Add child element to parent element
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                //create formData object from formTag
                const formData = new FormData(formTag);
                //convert formData to json
                const json = JSON.stringify(Object.fromEntries(formData));

                //send json data to create new location
                const locationUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
                }
            });


        }

    } catch {
        //figure out what to do if an error is raised
        const errorMessage = document.querySelector(".error")

        console.error(e);
        let htmlError = throwError(e);
        errorMessage.innerHTML += htmlError;
    }

});